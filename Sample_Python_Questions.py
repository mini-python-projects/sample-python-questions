#https://www.programiz.com/python-programming
#https://www.datacamp.com/community/tutorials/python-dictionary-comprehension

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Convert the entire string zenPython into a list of words. Capture the words in the list - words.
#Hint: Use split method of strings.
str = 'zenPython'
words = str.split("P")
print(words)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Now, remove the flanking characters (such as , . - * ! and space) from each of the word, present in list words.
#Hint: Use List comprehensions and strip method of strings.
words = ['!sourav','sahu','sourav*','---kumar']
words = [i.strip(', . - * ! ') for i in words]
print(words)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Convert all the words of list words to lower/upper case.
#Hint: Use list comprehensions and lower method of strings.
words = ['!SOUrav','SAHU','sourav*','---kumar','1991','Dob-1991']
words_lower = [i.lower() for i in words]
print(words_lower)
words_upper = [i.upper() for i in words]
print(words_upper)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Determine the unique set of words from the list words.
#Hint: Use set function
words = ['sourav','sahu','kumar','sahu','sourav','sourav']
unique_words = set(words)
print(unique_words)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Calculate the frequency of each identified unique word in the list - words and store the result in the dictionary word_frequency.
#Hint: Use dictionary comprehensions and count method of lists
words = ['sourav','sahu','sourav','kumar','sahu','sourav','sourav','sahu','sahu','sourav','kumar']
word_frequency = {i:words.count(i) for i in words}
print(word_frequency)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Create the dictionary frequent_words, which filter words having frequency greater than five from word_frequency.
#Hint : Use Dictionary comprehensions
frequent_words = {k:v for (k,v) in word_frequency.items() if v >= 5}
print(frequent_words)

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Define the generator function fib_gen, which is capable of yielding values of infinite fibonacci series.
#For e.g : You should able to create a generator fs from fib_gen and if fs is accessed using next for four times, then it should yield values 0, 1, 1, 2.
#To use yield generator must have loop inside itself
def fsg(n):
    x, y = 0, 1
    while x >= 0 and x <= n:
        yield x
        x, y = y, x+y
        
fs = fsg(50)

#next(fs)

#febo = [i for i in fs]
#print(febo)

for i in range(5):
    print(next(fs))

#-------------------------------------------------------------------------------------------------------------------------------------------------#
#Define the generator function factorial_gen, which is capable of yielding factorial values of natural numbers.
#For e.g : You should able to create a generator fs from factorial_gen and if fs is accessed using next for five times, then it should yield values 1, 1, 2, 6,24, 120,...

def fact(n):
    
    def fac(x):
        if x == 0:
            return 1
        return x * fac(x -1)
        
    for i in range(n + 1):
        yield fac(i)


fs = fact(5)

#facto = [i for i in fs]
#print(facto)

for i in range(5):
    print(next(fs))

